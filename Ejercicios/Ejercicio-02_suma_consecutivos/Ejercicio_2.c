#include <stdio.h>

int main(){
    int numero;
    printf("ingresa un numero entre 1 y 50: ");
    scanf("%d", &numero);
    if(numero > 50){    
        printf("numero muy grande\n");
        return 0;
    }
    if(numero < 1){
        printf("escriba un numero mayor o igual que 1\n");
        return 0;
    }
    int resultado = (numero *(numero + 1)) / 2; 
    printf("%i\n", resultado);
    
}