#include <stdio.h>

int main(){
    //consideraremos que el precio del dolar es de $20.00 pesos 
    float dolar = 20.00;
    float cant_dolares;
    float resultado = 0;
    printf("Cantidad de dolares a convertir: ");
    scanf("%f", &cant_dolares);
    resultado = dolar * cant_dolares;
    printf("%f dolares = ", cant_dolares);
    printf("%f pesos\n", resultado);
    return 0;
}