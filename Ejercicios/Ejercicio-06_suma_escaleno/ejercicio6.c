#include <stdio.h>

int main(){
    float lado1;
    float lado2;
    float lado3;
    float diferencia;
    printf("ingresa un numero positivo correspondiente a un lado del triangulo: ");
    scanf("%f", &lado1);
    printf("ingresa un numero positivo correspondiente al segundo lado del triangulo: ");
    scanf("%f", &lado2);
    if(lado1 < 0 | lado2 < 0){
        printf("ingresa solo numeros postivos\n");
        return 0;
    }
    if(lado1 >= lado2){
        diferencia = lado1 - lado2;
    }else{
        diferencia = lado2 - lado1;
    }
    printf("escoge un numero para el lado 3 en el intervalo (%f, ", diferencia);
    printf("%f): ", lado1 + lado2);
    scanf("%f", &lado3);
    if(lado3 < diferencia | lado3 > lado1 + lado2){
        printf("no se puede crear un triangulo escaleno con esas longitudes\n");
        return 0;
    }
    printf("El perimetro del triangulo escaleno es: %f \n", lado1 + lado2 + lado3);
}