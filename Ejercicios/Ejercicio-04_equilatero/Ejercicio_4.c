#include <stdio.h>

int main(){
    float lado;
    float resultado = 0;
    printf("ingresa el lado del triangulo equilatero: ");
    scanf("%f", &lado);
    if(lado < 0){
        printf("ingresa un numero positivo\n");
        return 0;
    }
    resultado = lado * 3;
    printf("el perimetro del triangulo equilatero de lado %f ", lado);
    printf("es %f \n", resultado);
}