#include <stdio.h>

int main(){
    float lado1;
    float lado2;
    float resultado;
    printf("ingresa el lado del tringulo que se repetira dos veces: ");
    scanf("%f", &lado1);
    if(lado1 < 0){
        printf("ingresa un numero positivo \n");
        return 0;
    }
    printf("ingresa el lado del triangulo que sera distinto, este se encuentra en el intervalo (0, %f): ", lado1+lado1);
    scanf("%f", &lado2);
    if(lado2 < 0){
        printf("ingresa un numero positivo \n");
        return 0;
    }
    if(lado2 > lado1 * 2){
        printf("No se puede crear un triangulo isosceles con esos valores \n");
        return 0;
    }
    resultado = (lado1 * 2) + lado2;
    printf("El perimetro del triangulo isosceles es %f \n", resultado);
}